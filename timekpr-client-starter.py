from urllib import request


# Updating the local config from a remote location to keep client devices in sync.
# Note that this could be a security issue depending on your setup.

# ADJUST to point to your local helper webserver - or remove if not needed.
remote_url = 'http://rex.local:9999/config'
local_file = 'conf/config.py'

try:
	request.urlretrieve(remote_url, local_file)
	print("Config file updated!")
except Exception  as e:
	print("ERROR: Could not update the config file from", remote_url)


# Entry point that loads the actual file.
from manager import timekpr_mqtt_client







