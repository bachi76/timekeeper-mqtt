
# Used topics and commands throughout the apps

## timekpr/command

### settimeleft
Example msg: `settimeleft:+:20`

Modes: "+", "-", "="

### points
Adds rewarded points, convert to additional computer time and add it.

### tick
Monitored devices need to send a tick signal every used minute. 
The server will count a minute as used if at least one tick was received.

Example msg: `tick`


## timekpr/log
Responses to commands, plus status info of the timekeeper (server)
- Online (retain=True)
- Success!
- OVER!
- Error!
- Offline (retain=True)

## timekpr/spent
Time spent today in minutes

## timekpr/left
Time left today in minutes



