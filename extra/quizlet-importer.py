
# Export quizlet question sets. They protect against scraping by JS, so we need to use a browser
# HowTo: Copy the source to the textfile, then execute this script

# README: => Im Quizlet auf das Bearbeiten / Speichern Icon gehen, und dann dort den Quelltext nehmen (Bsp. URL https://quizlet.com/782423467/autosaved)


import json

# Load text file from filesystem
def load_file(filename):
	with open(filename, 'r') as f:
		return f.read()


s = load_file('quizlet-html.txt')

start = 'window.Quizlet["createSetData"] = '
end = '; QLoad("Quizlet.createSetData");'
data = s[s.find(start)+len(start):s.rfind(end)]

json = json.loads(data)

#print(json['terms'][0]['word'])
#print(json['terms'][0]['definition'])

for item in json['terms']:
	q = item['word']
	a = item['definition']

	print(a + "\t" + q)
