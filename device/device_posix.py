import os

import psutil as psutil

import conf.config as cfg


def logout(timer):
	print("Logging out in ", timer)
	os.system('pkill -KILL -u {}'.format(cfg.user))


def user_is_active(username: str) -> bool:
	for user in psutil.users():
		if user.name == username:
			return True

	return False
