""" Acts as an interface to OS-specific functions """

import logging
import os
from plyer import notification


def logout(timer=0):
	raise "Not implemented"


def user_is_active(username: str) -> bool:
	raise "Not implemented"


def notify(title: str = '', text: str = '', timeout: int = 10):
	# notification.notify(title=title, message=text, timeout=timeout)
	# logging.info(f'Notify: "{title}" "{text}"')

	pass  # Boy doesn't like notifications...



# Map OS-specific functions to their implementation

if os.name == "nt":
	from device import device_nt
	logout = device_nt.logout
	user_is_active = device_nt.user_is_active

elif os.name == "posix":
	from device import device_posix

	logout = device_posix.logout
	user_is_active = device_posix.user_is_active

else:
	logging.error("Error: OS type '{}' not implemented".format(os.name))
