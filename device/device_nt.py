import logging
import os
import subprocess
import time

import conf.config as cfg
import ctypes


def logout(timer):
	print("Logging out in sofort", timer)
	time.sleep(timer)
	#os.system("C:\Windows\System32\shutdown.exe  -l -f")
	import ctypes

	# See https://docs.microsoft.com/de-de/windows/win32/api/winuser/nf-winuser-exitwindowsex?redirectedfrom=MSDN
	# Use EWX_LOGOFF and EWX_FORCE
	ctypes.windll.user32.ExitWindowsEx(0 | 0x00000004, 0)
	last_error = ctypes.GetLastError()
	print("ctypes last error:", last_error)
	logging.warning(f"ctype last error: {last_error}")


def user_is_active(username: str) -> bool:
	# TODO: Is psutils really crossplatform? Then fetch the user like on linux
	# For win, this is it! https://stackoverflow.com/questions/71854201/is-there-a-way-could-i-use-python-to-check-if-a-specific-user-is-logged-in-to-a

	# Windows Home is missing the query.exe, so we add the underlying quser.exe and call it directly.
	out = subprocess.getoutput('win-bins\\quser.exe')
	if cfg.user.lower() in out:
		logging.debug(f'{cfg.user} is logged in')
		return True
	else:
		logging.debug(f'{cfg.user} is logged out')
		return False



	# From https://stackoverflow.com/a/57258754

	# 10553666 - return code for unlocked workstation1
	# 0 - return code for locked workstation1
	#
	# 132782 - return code for unlocked workstation2
	# 67370 -  return code for locked workstation2
	#
	# 3216806 - return code for unlocked workstation3
	# 1901390 - return code for locked workstation3
	#
	# 197944 - return code for unlocked workstation4
	# 0 -  return code for locked workstation4

