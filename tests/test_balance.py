from trainer.balance import Balance


def test_balance_next():
	b = Balance()
	b.add([1, 2, 3])

	b.update(1, 1)
	b.update(2, 2)
	b.update(3, 3)

	assert b.next() == 1
	assert b.next(exclude=[1]) == 2
	b.update(1, 3)
	assert b.next() == 2
	assert b.next(exclude=[2, 3]) == 1


def test_available():
	b = Balance()
	b.add(["1", "2", "3"])

	assert set(b.available(1)) == {"1", "2", "3"}

	b.update("1", 3)
	b.update("2", 2)
	b.update("3", 0)

	assert set(b.available(2)) == {"2", "3"}

	b.update("3", 5)

	assert set(b.available(1)) == {"1", "2"}


