from trainer.quiz_base import QuizBase


class Q_Sub(QuizBase):
	def __init__(self):
		super().__init__("Test")
		self.x = 1
		self.y = 1
		self.loaded = False
		self.pers = False

		# Only persist x, not y
		self.persisted.append('x')

	def on_load(self):
		self.loaded = True

	def on_persist(self):
		self.pers = True


def test_persist():
	q1 = Q_Sub()
	q1.x = 2
	q1.y = 2
	q1.persist()
	assert q1.pers

	q2 = Q_Sub()
	q2.load()
	assert q2.x == 2
	assert q2.y == 1
	assert q2.loaded
	assert not q2.pers
