import dateutil
from bottle import route, run, template
import os
from pathlib import Path
import pandas as pd

# See https://github.com/sbi-rviot/ph_table
from pretty_html_table import build_table

# Needed for sentry remote error logging
import conf.config as cfg

from trainer import history


"""
A supporting webserver that will serve the config to other client, possibly more in the future.
Note that the config file should either contain only non-sensitive data and/or be accessible in a protected network only
"""


@route('/')
def index():
    df = history.get_dataframe()
    df['timestamp'] = df['timestamp'].apply(dateutil.parser.parse)
    df['count'] = 1

    df_history = df[['timestamp', 'name', 'percentage', 'questions', 'correct', 'time', 'earned_time', 'earn_vs_effort']].tail(20)[::-1]

    df_summary = df.groupby('name', as_index=False).aggregate({
        'name': 'first',
        'count': 'sum',
        'percentage': 'mean',
        'time': 'mean',
        'earned_time': 'mean',
        'earn_vs_effort': 'mean'
    })

    df_daily = df.groupby(pd.Grouper(key='timestamp', freq='D')).aggregate({
        'count': 'sum',
        'percentage': 'mean',
        'time': 'sum',
        'earned_time': 'sum',
        'earn_vs_effort': 'mean'
    })
    df_daily['date'] = df_daily.index
    df_daily = df_daily[['date', 'count', 'time', 'earned_time', 'earn_vs_effort']][::-1]

    report_history = build_table(df_history, 'blue_light')
    report_quiz = build_table(df_summary, 'red_light')
    report_daily = build_table(df_daily, 'green_light')

    return template(
        'index',
        template_settings={'noescape': True},
        report_quiz=report_quiz,
        report_history=report_history,
        report_daily=report_daily
    )


@route('/config')
def config():
    try:
        with open(path) as f:
            cfg = f.readlines()
            print(cfg)
            return cfg
    except Exception as e:
        print("Error while trying to serve config.py to client: ", e)
        return 404


# Quick startup test
print("Working dir: ", os.getcwd())
relpath = '../conf/config.py'

path = os.path.join(os.path.dirname(__file__), relpath)
if Path(path).is_file():
    print("Config file accessible")
else:
    print("ERROR: Config file not accessible in {}!".format(path))

run(host='0.0.0.0', port=9999)
