"""
This is the new server that will manage time and commands. It does no longer require and remote-control
the timekpr linux application.
"""
import datetime
import json
import logging
import pickle
import time
import traceback

import paho.mqtt.client as client

import conf.config as cfg
from trainer import history


# Run from project root folder with ' python3 -m manager.timekpr-mqtt-subscriber'

class Timekpr(object):
	pickle_path = "manager/data/timekpr.json"

	def __init__(self):
		self.log = None
		self.time = cfg.free_mins_per_day
		self.spent = 0
		self.points = 0
		self.date: datetime.datetime = datetime.datetime.today()

		# Multiple devices in parallel are allowed and counted as one.
		# If one client sets this to True during a given minute, the end-of-minute bookkeeping
		# will subtract that minute and set to False again.
		self.tick = False

		self.update()


	# Note: Pickling in a paho handler blocks (bug), thus we use a custom serialisation
	def __getstate__(self):
		return {
			'time': self.time,
			'spent': self.spent,
			'points': self.points,
			'date': self.date
		}


	# Must be called every minute
	def update(self):

		# If at least one client device was active this minute, count it as used
		if self.tick and self.time > 0:
			self.time -= 1
			self.spent += 1
			self.tick = False
			#logger.debug("Current minute counts as used.")

			if self.time == 0: # send only once
				logger.info(f'Time used up! ({self.spent}m used)')

			self.persist()

		# Reset stats at the beginning of a new day
		if self.date.day != datetime.datetime.today().day:
			logger.info(f'Today totals: spent: {self.spent}m, points: {self.points}, left: {self.time}m')

			# Update / reset stats for a new day
			self.date = datetime.datetime.today()
			self.time = cfg.free_mins_per_day
			self.spent = 0
			self.points = 0

			self.persist()


	@staticmethod
	def get_instance():
		"""
		If there is a persisted instance from today, reload that one, otherwise instantiate a new one.
		Kids find out about rebooting servers..
		"""
		timekpr = Timekpr()

		try:
			with open(Timekpr.pickle_path, "r") as handle:
				attribs = json.load(handle)

			attribs['date'] = datetime.datetime.fromisoformat(attribs['date'])

			if attribs['date'].day == datetime.datetime.today().day:
				for key, value in attribs.items():
					setattr(timekpr, key, value)
				logger.warning(f"Found an existing timekpr instance from today, using this one. {timekpr.spent} mins used / {timekpr.time} mins left today.")

		except Exception as e:
			logger.warning("No valid previous Timekpr pickle data found.", exc_info=e)

		finally:
			return timekpr


	def persist(self):
		"""
		Periodically persist state to protect against malicious reboots :)
		"""
		try:
			with open(self.pickle_path, "w") as handle:
				json.dump(timekpr.__getstate__(), handle, default=str)
				logger.info("Persisted timekpr")

		except Exception as e:
			logger.error('Error while trying to persist timekpr class!', exc_info=e)


def publish_stats():
	mqttc.publish(cfg.topic_log, "Online", retain=True)
	mqttc.publish(cfg.topic_left, timekpr.time, retain=True)
	mqttc.publish(cfg.topic_spent, timekpr.spent, retain=True)


def set_time_left(mode, time_requested):

	# Remotes use seconds for commands for historical reasons
	time_requested = int(int(time_requested) / 60)

	# Calc the max allowed time to set or add (in s)
	time_max = (cfg.max_mins_day - timekpr.spent)
	if mode == "+":
		time_max -= timekpr.time

	logger.debug("Requested: {}, Max left: {}".format(time_requested, time_max))

	# Cut request to max bounds
	time = min(time_requested, time_max)
	if mode == "=":
		timekpr.time = time
	elif mode == "+":
		timekpr.time += time
	elif mode == "-":
		timekpr.time -= time
		if timekpr.time < 0: timekpr.time = 0

	logger.info("Set time left: {}{}".format(mode, time))

	if time_requested == time:
		mqttc.publish(cfg.topic_log, "Success!")
	else:
		mqttc.publish(cfg.topic_log, "OVER!")

	publish_stats()
	timekpr.persist()

	return True


def on_connect(mqttc: client.Client, obj, flags, rc):
	logger.info("Connected to MQTT broker.")
	mqttc.subscribe(cfg.topic_command, qos=2)
	mqttc.subscribe(cfg.topic_results, qos=2)


def on_message(mqttc: client.Client, obj, msg):
	if msg.topic == cfg.topic_command:
		on_command(msg)

	if msg.topic == cfg.topic_results:
		on_result(msg)


def on_result(msg):
	payload = msg.payload.decode('ascii')
	logger.debug("Received: " + msg.topic + ": " + payload)

	try:
		data = json.loads(payload)
		history.add(data)
	except:
		logger.error(f"Error decoding result json payload: {payload}")


def on_command(msg):
	try:
		payload = msg.payload.decode('ascii')
		logger.debug("Received: " + msg.topic + ": " + payload)

		cmd = payload.split(":")

		# Example msg: "settimeleft:+:20". Modes: "+", "-", "="
		print(cmd[0])
		if str(cmd[0]) == 'settimeleft':
			mode = cmd[1]
			time = cmd[2]
			set_time_left(mode, time)

		# Example msg: "points:535"
		if str(cmd[0]) == 'points':
			points = int(cmd[1])
			time = points / cfg.points_per_minute * 60
			if set_time_left("+", time):
				logger.info("Added {}s for {} points.".format(time, points))

		# Example msg: "tick"
		if str(cmd[0]) == 'tick':
			logger.info("Tick received")
			timekpr.tick = True


	except Exception as e:
		logger.error(e, exc_info=True)


def on_publish(mqttc: client.Client, obj, mid):
	pass


def on_subscribe(mqttc: client.Client, obj, mid, granted_qos):
	#print("Subscribed: " + str(mid) + " " + str(granted_qos))
	pass


def on_log(mqttc: client.Client, obj, level, string):
	logger.debug(string)


logger = logging.getLogger("Server")
logger.info("Starting timekpr-mqtt...")

# Restore today's previous stats or get a new instance
timekpr = Timekpr.get_instance()

mqttc = client.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages (including exceptions from handlers!)
# mqttc.on_log = on_log
mqttc.will_set(cfg.topic_log, 'Offline', 0, retain=True)
mqttc.connect(cfg.mqtt_server, cfg.mqtt_port, 75)
# Subscribe in on_connect

mqttc.loop_start()

try:
	while True:
		timekpr.update()
		publish_stats()
		time.sleep(60)

except Exception as e:
	logger.error(traceback.format_exc())
	mqttc.loop_stop()
