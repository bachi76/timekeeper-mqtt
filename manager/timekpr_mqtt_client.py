"""
Use this on a client machine that listens to the timekeeper server (via mqtt topics).

Note: For Windows, make this a .pyw file and run it with pythonw.exe to
be able to run it in the background.

"""
import datetime
import time
import traceback

import paho.mqtt.client as client
import os
import logging
import socket

import conf.config as cfg
from device import device

dir_path = os.path.dirname(os.path.realpath(__file__))

# Define initial values
last_signal_from_server: datetime.datetime = datetime.datetime.now()
status_time_left = -99

client_id = socket.gethostname()
client_topic = f'{cfg.topic_clients}/{client_id}'


def client_msg(msg):
    """ Send a (retained) msg to the timekpr/clients/my-id/ topic"""
    mqttc.publish(client_topic, msg, retain=True, qos=1)


def on_connect(mqttc: client.Client, obj, flags, rc):
    mqttc.subscribe(cfg.topic_left, 0)
    mqttc.subscribe(cfg.topic_log, 0)

    client_msg("Connected")


def on_message(mqttc: client.Client, obj, msg):
    global last_signal_from_server, status_time_left

    payload = msg.payload.decode('ascii')

    if msg.topic == cfg.topic_left:
        left_new = int(float(payload))

        # Notify on changes, unless it's the normal -1 minute broadcast
        if left_new > status_time_left or left_new < status_time_left - 1:
            device.notify('Timekeeper', f'Verbleibende Zeit: {left_new} Minuten.')

        status_time_left = left_new
        last_signal_from_server = datetime.datetime.now()


def on_subscribe(mqttc: client.Client, obj, mid, granted_qos):
    pass


def on_log(mqttc: client.Client, obj, level, string):
    logging.debug(string)


# Logout if the user exits - only needed when running as the user itself
# atexit.register(device.logout, 0)

success = False
tries = 3
for t in range(tries):
    try:
        mqttc = client.Client(client_id=client_id)
        mqttc.will_set(client_topic, "Disconnected", qos=1, retain=True)
        mqttc.on_message = on_message
        mqttc.on_connect = on_connect
        mqttc.on_subscribe = on_subscribe
        mqttc.connect(cfg.mqtt_server, cfg.mqtt_port, 60)

        mqttc.loop_start()
        success = True
        break

    except Exception as e:
        logging.error(traceback.format_exc())
        device.notify('Timekeeper Problem', f'Problem mit der Verbindung, Versuch {t+1} von {tries}', 10)
        time.sleep(20)

if not success:
    logging.error("Could not connect to MQTT broker - giving up after 3 tries!")
    device.notify('Timekeeper Problem', 'Kann nicht zum Timekeeper-Server verbinden - Ausloggen erfolgt in 20s.')
    device.logout(20)
    exit()

logging.info("Timekeeper ist aktiv :)")


try:
    while True:
        if status_time_left != -99:
            logging.debug(f"Time left: {status_time_left}, last signal: {last_signal_from_server}")

        if device.user_is_active(cfg.user):

            # Send out ticks if the device is actively used.
            mqttc.publish(cfg.topic_command, "tick", qos=1)

            if status_time_left == 10:
                device.notify('Timekeeper', 'Es verbleiben noch 10 Minuten.', 30)

            if status_time_left == 3:
                device.notify('Timekeeper', 'Es verbleiben noch 3 Minuten...', 30)

            if status_time_left == 1:
                device.notify('Achtung!', 'Die letzte Minute tickt...', 50)

            # Time is over - log out
            if status_time_left < 1 and status_time_left != -99:
                delay = 30
                device.notify('Die Zeit ist abgelaufen!', f'Du wirst in {delay}s ausgeloggt.', delay - 5)
                client_msg("Logout (time over)")
                device.logout(delay)

            # Check if the server is online and log out if it is not
            last_signal = int((datetime.datetime.now() - last_signal_from_server).seconds / 60)
            if last_signal >= 2:
                device.notify('Keine Verbindung zum Timekeeper', 'Logout erfolgt in 3 Minuten...')

            if last_signal >= 3:
                device.notify('Keine Verbindung zum Timekeeper', 'Logout in 30 Sekunden...', 30)
                client_msg("Logout (conn. loss)")
                device.logout(30)

            # Rinse and repeat every minute
            time.sleep(60)

        else:
            # Recheck if user is online now
            time.sleep(10)

except Exception as e:
    logging.error(traceback.format_exc())
    client_msg("Error")
    mqttc.loop_stop()

# logout(10)
