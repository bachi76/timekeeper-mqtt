"""
This is the old / deprecated script that connects to the timekpr app on a linux box.

"""

import os
import subprocess
import logging
import time
import traceback
import sys

import paho.mqtt.client as client

# Run from project root folder with ' python3 -m manager.timekpr-mqtt-subscriber'

import conf.config as cfg


class Timekpr(object):
	def __init__(self):
		self.log = None
		self.time = 0
		self.spent = 0

timekpr = Timekpr()

logger = logging.getLogger("Manager")

def publish_stats():
	mqttc.publish(cfg.topic_log, "Online", retain=True)

	info = userinfo(cfg.user)
	timekpr.time = int(int(info['TIME_LEFT_DAY']) / 60)
	timekpr.spent = int(int(info['TIME_SPENT_DAY']) / 60)

	mqttc.publish(cfg.topic_left, timekpr.time, retain=True)
	mqttc.publish(cfg.topic_spent, timekpr.spent, retain=True)


def userinfo(user):
	result: subprocess.CompletedProcess = subprocess.check_output(["timekpra", "--userinfo", user])
	out = result.decode(sys.stdout.encoding).splitlines()

	userinfo = {}
	for line in out:
		if ": " in line:
			k, v = line.split(": ")
			userinfo[k] = v

	# Some fields are not always set
	if 'TIME_LEFT_DAY' not in userinfo:
		userinfo['TIME_LEFT_DAY'] = '0'

	if 'TIME_SPENT_DAY' not in userinfo:
		userinfo['TIME_SPENT_DAY'] = '0'

	return userinfo


def set_time_left(mode, time_requested):

	# Calc the max allowed time to set or add (in s)
	time_max = (cfg.max_mins_day - timekpr.spent) * 60
	if mode == "+":
		time_max -= timekpr.time * 60

	#logger.debug("Requested: {}, Max left: {}".format(time_requested, time_max))

	# Cut request to max bounds
	time = str(min(int(time_requested), time_max))

	result: subprocess.CompletedProcess = subprocess.run(["timekpra", "--settimeleft", cfg.user, mode, time])
	if result.returncode == 0:  # timekpra doesn't return non-0 when an error happens :-/
		logger.info("Set time left: {}{}".format(mode, time))

		if time_requested == time:
			mqttc.publish(cfg.topic_log, "Success!")
		else:
			mqttc.publish(cfg.topic_log, "OVER!")

		publish_stats()
		return True

	else:
		logger.error("Returncode was non-zero")
		mqttc.publish(cfg.topic_log, "Error!")
		return False


def on_connect(mqttc, obj, flags, rc):
	logger.info("Connected to MQTT broker.")
	mqttc.subscribe(cfg.topic_command, 2)


def on_message(mqttc, obj, msg):
	try:
		payload = msg.payload.decode('ascii')
		logger.debug("Received: " + msg.topic + ": " + payload)

		cmd = payload.split(":")

		# Example msg: "settimeleft:+:20". Modes: "+", "-", "="
		print(cmd[0])
		if str(cmd[0]) == 'settimeleft':
			mode = cmd[1]
			time = cmd[2]
			set_time_left(mode, time)

		# Example msg: "points:535"
		if str(cmd[0]) == 'points':
			points = int(cmd[1])
			time = points / cfg.points_per_minute * 60
			if set_time_left("+", time):
				logger.info("Added {}s for {} points.".format(time, points))

	except Exception as e:
		logger.error(e, exc_info=True)


def on_publish(mqttc, obj, mid):
	pass


def on_subscribe(mqttc, obj, mid, granted_qos):
	#print("Subscribed: " + str(mid) + " " + str(granted_qos))
	pass

def on_log(mqttc, obj, level, string):
	logger.debug(string)

logger.info("Starting timekpr-mqtt...")

mqttc = client.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages (including exceptions from handlers!)
# mqttc.on_log = on_log
mqttc.will_set(cfg.topic_log, 'Offline', 0, retain=True)
mqttc.connect(cfg.mqtt_server, cfg.mqtt_port, 60)
# Subscribe in on_connect

mqttc.loop_start()

try:
	while True:
		publish_stats()
		time.sleep(60)


except Exception as e:
	logger.error(traceback.format_exc())
	mqttc.loop_stop()
