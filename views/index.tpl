<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Timekeeper Quiz Trainer Stats</title>
</head>

<body>
  <h1>Timekeeper Quiz Trainer Stats</h1>
  <h2>Quiz Stats</h2>
  {{report_quiz}}

  <h2>Quiz History</h2>
  {{report_history}}

  <h2>Daily stats</h2>
  {{report_daily}}

</body>
</html>
