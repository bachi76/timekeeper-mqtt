import json
from datetime import datetime


class Result:
	def __init__(self, name, questions, correct, time, points):
		self.points = points
		self.time = round(time / 60, 1)
		self.correct = correct
		self.questions = questions
		self.name = name

		self.earned_time = 0
		self.earn_vs_effort = 0

		self.percentage = round(correct / self.questions * 100)
		self.timestamp = str(datetime.now().isoformat(' ', 'minutes'))


	def earned(self, mins):
		self.earned_time = mins
		if self.time > 0:
			self.earn_vs_effort = round(self.earned_time / self.time, 2)

	def to_json(self) -> str:
		return json.dumps(self.__dict__)
