from datetime import datetime


class Stats:
	def __init__(self):
		self.total_runs = 0
		self.total_time = 0
		self.total_points = 0
		self.runs = []

	def add(self, stat):
		self.total_runs += 1
		self.total_time += stat.time
		self.total_points += stat.points
		self.runs.append(stat)

	def last(self):
		if len(self.runs) > 0:
			return self.runs[-1]
		else:
			return None

class Stat:
	def __init__(self, time, points):
		self.date = datetime.now()
		self.time = time
		self.points = points