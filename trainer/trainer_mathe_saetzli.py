
from rich import print
import pandas as pd

from trainer.trainer_gsheet_base import TrainerGsheetBase
from random import randint, randrange
from fractions import Fraction

import logging

class Trainer(TrainerGsheetBase):
	"""
	A math trainer that allows for dynamic number parameters in the text. The answer as formula (e.g. b/c*a) and calculated
	when run. The parameters (variables) a-f are int 1-20 by default, or can be defined in the spreadsheet.
	"""
	def __init__(self):
		super(Trainer, self).__init__("Mathe Sätzliaufgaben")

		self.name = "Mathe+"
		self.desc = "Die reale Welt berechnen."

		# Google sheet: https://docs.google.com/spreadsheets/d/1yBvLqBpEKY-BeLPukEQVJjYsb9cDKuvCiI41zuKbd7s/edit#gid=1187028488
		# Question sheet
		self.questions_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQPxx1BTgxXKdHqqrTRPXrkYbmmO3CAtpHa_TFBJf0ipJVuWadsx2oTZkRYFrP8klwZzny6wfEGktAr/pub?gid=342004443&single=true&output=tsv"

		# TEST SHEET, DO NOT DEPLOY
		# self.questions_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQPxx1BTgxXKdHqqrTRPXrkYbmmO3CAtpHa_TFBJf0ipJVuWadsx2oTZkRYFrP8klwZzny6wfEGktAr/pub?gid=820404273&single=true&output=tsv"

		self.questions_per_set = self.get_config('questions_per_set', 3)
		self.points_per_full_answer = self.get_config('points_per_full_answer', 30)
		self.points_per_half_answer = self.get_config('points_per_half_answer', 15)


	def on_before_question(self, question, answer, params):

		try:

			# Fill the question params, either using the given expression, or random 1-20
			for i in range(0, 5):
				if not pd.isna(params[i]) and len(str(params[i])) > 0:
					# FIXME: Sanity check!
					params[i] = eval(str(params[i]))
				else:
					params[i] = randint(1, 20)

				# Replace the variable names in question and answer. Ensure string!
				question = str(question).replace('{' + params.index[i] + '}', str(params[i]))
				answer = str(answer).replace('{' + params.index[i] + '}', str(params[i]))

			# Parse the result
			# FIXME: Sanity check!
			answer = str(eval(answer))
			# 456.0 -> 456
			if answer.endswith('.0'):
				answer = answer.replace('.0', '')

			# print(answer)
			return question, answer

		except Exception as ex:
			raise SyntaxError(f'Question parsing error for question: "{question}"') from ex


""" Test run """
if __name__ == '__main__':


	t = Trainer()
	t.load_data()

	while True:
		q, a = t.get_next_question()
		q, a = t.on_before_question(q, a, t.get_question_params(q).copy())
		print(q)
		print(a)
		print('\n\n')
