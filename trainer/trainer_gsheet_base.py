import logging
import time

import textdistance as textdistance
from rich.prompt import Prompt
from rich import print

from trainer import result
from trainer.balance import Balance
from trainer.quiz_base import QuizBase
from trainer.result import Result
from trainer.stats import Stat
import pandas as pd


class TrainerGsheetBase (QuizBase):
	def __init__(self, name):

		super(TrainerGsheetBase, self).__init__(name)

		self.name = ""
		self.desc = ""

		# Gsheet public url (must be TSV-format!)
		self.questions_url = ""

		self.questions_per_set = 6
		self.points_per_full_answer = 15
		self.points_per_half_answer = 8

		self.df_questions: pd.DataFrame = pd.DataFrame()

		self.balancer = Balance()
		self.persisted.append('balancer')


	def on_before_question(self, question, answer, params):
		""" Use this event to modify the answer and question in subclasses. """
		return question, answer


	def get_question_params(self, question):
		"""
		Get the question's params (that is, columns C-G in the spreadsheet.
		Usage: params.a (a-e)
		"""
		return self.df_questions.loc[question][1:]


	def load_data(self):
		# Note: Google updates only every 5 mins
		# Question is the index, answer is required, take up to 5 additional params, a - e
		self.df_questions = pd.read_csv(self.questions_url, sep="\t", header=None, index_col=0, names=["question", "answer", "a", "b", "c", "d", "e"])

		# Remove duplicate questions, keeping the last
		self.df_questions = self.df_questions[~self.df_questions.index.duplicated(keep='last')]

		self.balancer.add(self.df_questions.index.tolist())


	def get_next_question(self):
		""" Get the next question from balancer that has a current answer in the Google sheet. Restart session if we're out of questions. """

		while True:
			question = self.balancer.next()

			# If no more unique questions are left, start a new session
			if not question:
				self.balancer.start_session()
				question = self.balancer.next()

			if question in self.df_questions.index:
				break

		answer = self.df_questions.loc[question].answer

		return question, answer


	def run(self) -> Result:
		self.load_data()

		start = time.time()
		points = 0
		correct = 0

		# print("Scores so far:")
		# print(self.balancer.data)
		print()

		self.balancer.start_session()

		for i in range(self.questions_per_set):
			question, answer = self.get_next_question()

			try:
				question, answer = self.on_before_question(question, answer, self.get_question_params(question).copy())
				print("\n")
				response = Prompt.ask(question)

			except Exception as e:
				print("Autsch, ein techn. Problem mit einer Frage..")
				logging.exception(e)
				continue

			if response == answer:
				points += self.points_per_full_answer
				self.balancer.update(question, self.points_per_full_answer)
				correct += 1
				print("[green]Korrekt.[/green]")

			else:
				# TODO clean-up and have proper eval events and string/number handling
				if isinstance(answer, str):
					similarity = textdistance.hamming.normalized_similarity(response, answer)
					if similarity >= 0.75:
						print("[bold red]Oopsie, leider nicht ganz korrekt..[/bold red]")
					else:
						print("[bold red]Oopsie, leider falsch..[/bold red]")
					response2 = Prompt.ask(question + " ([blue]{}{}[/blue])".format(answer[0], (len(answer) - 1) * "."))
				else:
					response2 = Prompt.ask(question)

				if response2 == answer:
					points += self.points_per_half_answer
					self.balancer.update(question, self.points_per_half_answer)
					correct += 0.5
					print("[green]Korrekt.[/green]")

				else:
					print("[red]Nope - die richtige Antwort lautet: [/red][blue bold]{}[/blue bold]".format(answer))
					time.sleep(3)

		used = round(time.time() - start)

		print()
		print("Zeit gebraucht: {}s".format(used))
		print("Richtige: [green]{}[/green] / Falsche: [red]{}[/red]".format(correct, self.questions_per_set - correct))
		print("Zeit total: {}s".format(used))

		payload = {
			"name": self.name,
			"questions": self.questions_per_set,
			"correct": correct,
			"wrong": self.questions_per_set - correct,
			"percentage": round(correct / self.questions_per_set * 100),
			"time": used,
			"points": points
		}

		s = Stat(used, points)
		self.stats.add(s)
		self.balancer.end_session()

		return result.Result(
			name=self.name,
			questions=self.questions_per_set,
			correct=correct,
			time=used,
			points=points
		)
