import random
import time

from rich.prompt import IntPrompt
from rich import print

from trainer.quiz_base import QuizBase
from trainer.result import Result
from trainer.stats import Stats, Stat
from trainer.balance import Balance


class Trainer (QuizBase):
	def __init__(self):
		super(Trainer, self).__init__("Einmaleins")

		self.name = "Einmaleins"
		self.desc = "Schöne ganze GROSSE Reihen"

		# self.series = [3, 4, 6, 7, 8, 9, 11, 12, 15, 25]
		self.series = [7, 9, 12, 13, 14, 15, 16, 17, 18, 19]

		self.up_to = 12
		self.random = False
		self.error_penalty = 5

		self.balancer = Balance(self.series)
		self.persisted.append('balancer')

		self.last = None


	def run(self) -> Result:
		# print("Aktuelle Reihen-Bilanz:", self.balancer.data)
		# print()

		start = time.time()
		errors = 0

		#b = random.choice(self.series)
		reihe = self.balancer.next(exclude=[self.last])

		for a in range(1, self.up_to + 1):
			r = 0
			while r != a * reihe:
				r = IntPrompt.ask("{} x {} = ".format(a, reihe))

				if r != a * reihe:
					print("[bold red]Oopsie, leider falsch..[/bold red]")
					errors += 1

		used = round(time.time() - start)
		penalty = errors * self.error_penalty
		total = used + penalty

		print()
		print("Zeit gebraucht: {}s".format(used))
		print("Fehler: {} -> +{}s".format(errors, penalty))
		print("Zeit total: {}s".format(total))

		points = max(160 - total, 0)  # Fixme machwasschlaues.. War: 80 bei kleinen Reihen
		s = Stat(total, points)
		self.stats.add(s)
		self.balancer.update(reihe, points)
		self.last = reihe

		return Result(
			name=self.name,
			questions=self.up_to,
			correct=self.up_to - errors,
			time=used,
			points=points
		)



