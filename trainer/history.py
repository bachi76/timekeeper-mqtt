import logging
import pickle

from pathlib import Path

import pandas as pd

data_path = 'trainer/data/trainer_history.pickle'
logger = logging.getLogger("history")


def add(record: dict):
	""" Add a result to the history and store it """

	data = get()
	data.append(record)
	try:
		with open(data_path, 'wb') as handle:
			pickle.dump(data, handle)
			logger.info(f'New quiz result added: {record}. History contains {len(data)} records now')
	except:
		logger.error(f'Cannot pickle history to {data_path}', exc_info=True)


def get() -> list:
	""" Load the history from disk and return as list of Result"""

	if Path(data_path).is_file():
		try:
			with open(data_path, 'rb') as handle:
				data = pickle.load(handle)
				return data
		except:
			logger.error(f'Cannot unpickle history from {data_path}', exc_info=True)
			return []

	return []


def get_dataframe() -> pd.DataFrame:
	""" Get the history as pandas Dataframe """
	return pd.DataFrame.from_dict(get())


# r = Result("bla", 5, 3, 14, 12)
# add(r.__dict__)
# print(get())
