import time

import textdistance as textdistance
from rich.prompt import Prompt
from rich import print

from trainer.balance import Balance
from trainer.quiz_base import QuizBase
from trainer.stats import Stat
import pandas as pd

from trainer.trainer_gsheet_base import TrainerGsheetBase


class Trainer(TrainerGsheetBase):
	def __init__(self):
		super(Trainer, self).__init__("Latin-Formen")

		self.name = "Latin-Formen"
		self.desc = "Per aspera ad astra."

		# Google sheet: https://docs.google.com/spreadsheets/d/14C7Dj1O_VoCZTT_Znpylicgauvhi9EiHyRfn7HyETUQ/edit?gid=1282994935#gid=1282994935
		self.questions_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRy2bwDvhaGCzlmOkQUAmEQq4Wu4YMzI2v-trlEE8vbIio1AIV00y-ow3R6Ke9e1rvXumFGRjo6i4ex/pub?gid=1282994935&single=true&output=tsv"

		self.questions_per_set = self.get_config('questions_per_set', 6)
		self.points_per_full_answer = self.get_config('points_per_full_answer', 30)
		self.points_per_half_answer = self.get_config('points_per_half_answer', 15)
