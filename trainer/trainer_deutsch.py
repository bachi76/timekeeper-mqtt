from trainer.trainer_gsheet_base import TrainerGsheetBase


class Trainer (TrainerGsheetBase):
	def __init__(self):
		super(Trainer, self).__init__("Deutsch")

		self.name = "Deutsch"
		self.desc = "Vom Präteritum bis zum perfekten Futur."

		# Google sheet: https://docs.google.com/spreadsheets/d/1yBvLqBpEKY-BeLPukEQVJjYsb9cDKuvCiI41zuKbd7s/edit#gid=0
		self.questions_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQPxx1BTgxXKdHqqrTRPXrkYbmmO3CAtpHa_TFBJf0ipJVuWadsx2oTZkRYFrP8klwZzny6wfEGktAr/pub?gid=0&single=true&output=tsv"

		self.questions_per_set = self.get_config('questions_per_set', 3)
		self.points_per_full_answer = self.get_config('points_per_full_answer', 10)
		self.points_per_half_answer = self.get_config('points_per_half_answer', 5)

