import logging
import logging as log
import pickle

import numpy
import pandas as pd

from trainer.result import Result
from trainer.stats import Stats
import conf.config as cfg

class QuizBase:

	pd_gsheet_config = None

	def __init__(self, key):
		self.name = ''
		self.key = key
		self.desc = ''

		self.stats = Stats()
		self.logger = logging.getLogger(self.key)
		self.data_file = "trainer/data/" + self.key + ".pickle"

		# The list of attributes that should be persisted to disk (pickled)
		self.persisted = ['stats']


	def run(self) -> Result:
		""" Entry point, overwrite in subclasses"""
		log.debug('Superclass run')
		return None


	def load(self):
		""" Load (and overwrite existing) persisted attribs from disk """
		persisted = {}
		try:
			persisted = pickle.load(open(self.data_file, "rb"))
			self.logger.debug("Loaded persisted attributes from " + self.data_file)

		except Exception:
			self.logger.debug("No persisted attributes found for " + self.__class__.__name__)

		# Overwrite attribs set in __init__ with those persisted
		self.__dict__.update(persisted)
		self.on_load()


	def persist(self):
		""" Persist all attributes listed in self.persisted """
		self.on_persist()

		# Persist only the attribs defined in self.persisted
		persist = {k: self.__dict__.get(k) for k in self.__dict__ if k in self.persisted}

		try:
			pickle.dump(persist, open(self.data_file, "wb+"))
		except Exception as e:
			self.logger.error("Could not persist to " + self.data_file, e, stack_info=True)


	def get_config(self, key, default):
		""" Get a config value from the config gsheet for this quiz, or return the default if not present """
		df = QuizBase.pd_gsheet_config
		if df is not None and self.name in df and key in df[self.name]:
			val = df[self.name][key]

			# Return float or int, not numpy.float64
			if isinstance(val, numpy.float64):
				val = float(val)
				if val.is_integer():
					val = int(val)

			return val

		else:
			return default


	def on_persist(self):
		"""Called before persisting data"""
		pass


	def on_load(self):
		"""Called after persisted data was loaded"""
		pass


# Load the trainer config gsheet. Only once per session.
if cfg.config_gsheet_url:
	try:
		QuizBase.pd_gsheet_config = pd.read_csv(cfg.config_gsheet_url, sep="\t", index_col=0)
		logging.getLogger().info("Loaded gsheet remote config")

	except Exception as e:
		logging.getLogger().error("Error loading remote gsheet config", exc_info=e)
