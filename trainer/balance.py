import random

class Balance:
	def __init__(self, elements=[]):
		self.data = {}
		self.add(elements)
		self.in_session = False
		self.session = []

	def add(self, new_elements: list, balance=False, balance_below_min=1):
		""" Add new elements/questions to be balanced. If balance is True, set new questions to
		the value of the element/question with the lowest points, minus balance_below_min """
		if balance:
			val = max(0, self._min_value() - balance_below_min)
		else:
			val = 0

		for e in new_elements:
			if e not in self.data:
				self.data[e] = val

	def set(self, set_elements: list, balance=False, balance_below_min=1):
		""" Set elements. If they existed before, keep their counts. New elements are added,
		elements not present in the list are removed. """

		# Add new elements if needed, remove existing ones not present in new list
		for e in set_elements:
			if e not in self.data:
				self.add([e], balance, balance_below_min)
		existing = list(self.data.keys()).copy()
		for e in existing:
			if e not in set_elements:
				self.data.pop(e)



	def update(self, key, points):
		""" Update the points for an existing (or add a new) element """
		if key in self.data:
			self.data[key] += points
		else:
			self.data[key] = points


	def next(self, exclude=None, unique_in_session=True):
		""" Get the element with the lowest points (and not in th exclude list or already returned this session).
			Returns None if no element is left. """

		if exclude is None:
			exclude = []

		if self.in_session:
			exclude += self.session

		d = [d for d in self.data.keys() if d not in exclude]
		random.shuffle(d)

		if len(d) < 1:
			#raise ValueError("No data left to select from!")
			return None

		# print(self.data)
		result = min(d, key=(lambda k: self.data[k]))
		if self.in_session:
			self.session.append(result)

		return result


	def start_session(self):
		""" Start a new session (calls to next() will return unique elements)."""
		self.in_session = True
		self.session = []


	def end_session(self):
		""" End the current session (resets the list of already retuned elements) """
		self.in_session = False
		self.session = []


	def available(self, max_distance_from_min_points):
		""" Get a list of questions that are within the max. distance of the min point element. """
		minval = self._min_value() + max_distance_from_min_points
		return [key for key in self.data if self.data[key] <= minval]


	def _min_value(self):
		""" Get the lowest value from the history """
		return min(self.data.values()) if len(self.data) > 0 else 0
