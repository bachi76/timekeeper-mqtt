import logging
import importlib
import pickle
import time

import paho.mqtt.client as client
import os

# https://github.com/willmcgugan/rich / https://github.com/willmcgugan/rich
from rich import print
from rich.console import Console
from rich.panel import Panel
from rich.prompt import IntPrompt, Prompt

import conf.config as cfg
from trainer import history
from trainer.balance import Balance
from trainer.quiz_base import QuizBase
from trainer.stats import Stat
from trainer.result import Result

balancer_path = "trainer/data/trainer_balancer.pickle"
admin_mode = False


class Timekpr(object):
	def __init__(self):
		self.log = None
		self.time = 0
		self.spent = 0


def on_connect(mqttc, timekpr, flags, rc):
	logger.info("Connected to MQTT broker.")
	mqttc.subscribe(cfg.topic_left, 0)
	mqttc.subscribe(cfg.topic_spent, 0)
	mqttc.subscribe(cfg.topic_log, 0)


def on_message(mqttc, timekpr, msg):
	payload = msg.payload.decode('ascii')

	if msg.topic == cfg.topic_left: timekpr.time = int(float(payload))
	if msg.topic == cfg.topic_spent: timekpr.spent = int(float(payload))
	if msg.topic == cfg.topic_log: timekpr.log = payload


def on_publish(mqttc, obj, mid):
	pass


def scnclr():
	""" Clear the screen """
	if os.name == 'nt':
		_ = os.system('cls')
	else:
		_ = os.system('clear')


if __name__ == '__main__':
	logger = logging.getLogger("Trainer")
	logger.info("Starting training session.")

	#print("Heute morgen reicht die Zeit ja auch so :)")
	#time.sleep(3)
	#exit()

	# Load quiz balancer
	try:
		balancer = pickle.load(open(balancer_path, "rb"))
	except Exception:
		logger.warning("No quiz balancer data found, generating a new one.")
		balancer = Balance()

	# Import training modules
	trainers = []
	for t in cfg.trainers:
		m = importlib.import_module("trainer.trainer_" + t)
		trainers.append(m.Trainer())

	# Add the trainer keys to the balancer
	balancer.set([t.key for t in trainers], balance=True, balance_below_min=6) # was: 3
	# FIXME: Create a balancer.set() which sets a give set from cfg, keeping amounts and balancing
	# print("Quiz data:")
	print(balancer.data)
	# time.sleep(3)

	# MQTT setup
	timekpr = Timekpr()
	mqttc = client.Client(userdata=timekpr)
	mqttc.on_message = on_message
	mqttc.on_connect = on_connect
	mqttc.on_publish = on_publish
	# mqttc.on_subscribe = on_subscribe
	# mqttc.on_log = on_log
	mqttc.connect(cfg.mqtt_server, cfg.mqtt_port, 60)
	# Subscribe in on_connect

	mqttc.loop_start()

	while True:

		txt_main = "Hallo [red bold]{}[/red bold], parat für ein Spiel?".format(cfg.user.title())
		txt_main += "\n\nFolgendes steht zur Auswahl:\n\n"

		i = 0
		available = balancer.available(cfg.max_quiz_distance)
		available_keys = [99]  # 99 being a easter egg..

		for t in trainers:
			t: QuizBase
			i += 1
			if t.key in available:
				available_keys.append(i)
				txt_main +="[red bold]{}.[/red bold] [blue bold]{}[/blue bold] [grey italic]({})[/grey italic]\n".format(i, t.name, t.desc)
			else:
				txt_main += "[grey bold]{}.[/grey bold] [grey bold]{}[/grey bold] [grey italic]({})[/grey italic]\n".format(i, t.name, t.desc)

		scnclr()

		if admin_mode:
			print("Quiz data:")
			print(balancer.data, "\n")

		print(Panel(txt_main, title="Quiz Master 1.8 by Papa"))

		sel = 0
		while sel not in available_keys and not (admin_mode and sel != 0):
			sel = IntPrompt.ask("Was darfs sein{}? ".format("" if not admin_mode else ", Admin"))

		if sel == 99:
			admin_mode = not admin_mode
			print("Admin mode: ", admin_mode)
			time.sleep(1)
			continue

		# Stop if time used + left is nearing (10m earlier) the daily max.
		if timekpr.spent + timekpr.time + 10 >= cfg.max_mins_day and not admin_mode:
			print("\n[green bold] Für heute ist es genug, Zeit für etwas anderes :-)[/green bold]\n")
			time.sleep(3)
			exit()

		selected_trainer: QuizBase
		selected_trainer = trainers[sel - 1]

		# Print trainer stats if in admin mode
		if admin_mode:
			selected_trainer.load()
			print(selected_trainer.balancer.data)
			print()
			Prompt.ask("Drücke <Enter> für Weiter")
			# continue

		logger.info("Starting {} trainer.".format(selected_trainer.name))
		console = Console()
		with console.status("Lade [blue bold]{}[/blue bold]...".format(selected_trainer.name)) as status:
			time.sleep(1.5)
			print("Fertig!")

		#time.sleep(1)
		scnclr()

		# Load data, run, persist data
		selected_trainer.load()
		result = selected_trainer.run()
		selected_trainer.persist()

		# Count the completed quiz - if the percentage is high enough
		if result.percentage >= cfg.min_percentage_to_count_quiz:
			balancer.update(selected_trainer.key, 1)
		else:
			logger.warning("Quiz not counted, because the percentage was too low: {}%".format(result.percentage))


		try:
			pickle.dump(balancer, open(balancer_path, "wb+"))
		except Exception as e:
			logger.error("Could not persist to " + balancer_path, e, stack_info=True)

		# Stat could be replaced by the newer Result
		stats: Stat = selected_trainer.stats.last()
		addtime = round(stats.points / cfg.points_per_minute)
		if stats.points > 40:
			adj = "fette"
		elif stats.points > 20:
			adj = "satte"
		elif stats.points > 10:
			adj = "gemütliche"
		else:
			adj = "etwas magere"

		print(Panel("Das waren {} {} Punkte, {}!".format(adj, stats.points, cfg.user.title())))
		logger.info("Completed {} with {} points in {}s".format(selected_trainer.name, stats.points, stats.time))

		with console.status("Gamezeit anpassen: +{} Minuten...".format(addtime)) as status:
			timekpr.log = ''
			if not admin_mode:
				mqttc.publish(cfg.topic_command, "points:{}".format(round(stats.points)), qos=2)
			time.sleep(1)
			print("gemacht!")

		# Publish the results. The server part will store them.
		result.earned(addtime)
		if not admin_mode:
			mqttc.publish(cfg.topic_results, result.to_json(), qos=2)

		print()
		Prompt.ask("Drücke <Enter> für Weiter")
