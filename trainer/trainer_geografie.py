import time

import textdistance as textdistance
from rich.prompt import Prompt
from rich import print

from trainer.balance import Balance
from trainer.quiz_base import QuizBase
from trainer.stats import Stat
import pandas as pd

from trainer.trainer_gsheet_base import TrainerGsheetBase


class Trainer(TrainerGsheetBase):
	def __init__(self):
		super(Trainer, self).__init__("Geografie")

		self.name = "Geografie"
		self.desc = "Was ist wo auf unserer Erde?"

		# Google sheet: https://docs.google.com/spreadsheets/d/1yBvLqBpEKY-BeLPukEQVJjYsb9cDKuvCiI41zuKbd7s/edit#gid=1523306557
		self.questions_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQPxx1BTgxXKdHqqrTRPXrkYbmmO3CAtpHa_TFBJf0ipJVuWadsx2oTZkRYFrP8klwZzny6wfEGktAr/pub?gid=1523306557&single=true&output=tsv"

		self.questions_per_set = self.get_config('questions_per_set', 5)
		self.points_per_full_answer = self.get_config('points_per_full_answer', 18)
		self.points_per_half_answer = self.get_config('points_per_half_answer', 9)
