import random
import time

from rich.prompt import IntPrompt
from rich import print

from trainer.quiz_base import QuizBase
from trainer.result import Result
from trainer.stats import Stats, Stat
from trainer.balance import Balance


class Trainer (QuizBase):
	def __init__(self):
		super(Trainer, self).__init__("Division")

		self.name = "Division"
		self.desc = "Teilen ist nobel und edel."

		# Calcs per round
		self.num_calcs = self.get_config('num_calcs', 10)

		# a and b of a x b
		self.series = [3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15]
		self.up_to = 12

		# Points = max_time - actual_time
		self.points_max_time = self.get_config('points_max_time', 12)

		# Penalty seconds for wrong answers
		self.error_penalty = self.get_config('error_penalty', 3)

		self.balancer = Balance()
		self.persisted.append('balancer')

		# Generate all calculations
		items = []
		for i in self.series:
			for j in range(2, self.up_to-1):
				items.append("{} : {}".format(i*j, j))

		self.balancer.add(items)

		self.last = None


	def run(self) -> Result:
		#print("Aktuelle Reihen-Bilanz:", self.balancer.data)
		#print()

		total_points = 0
		total_time = 0
		total_errors = 0

		self.balancer.start_session()

		for i in range(self.num_calcs):
			calc = self.balancer.next(exclude=[self.last])
			(a, b) = calc.split(" : ")
			a = int(a)
			b = int(b)
			start = time.time()
			errors = 0

			r = 0
			while r != a / b:
				r = IntPrompt.ask("{} : {} = ".format(a, b))

				if r != a / b:
					print("[bold red]Oopsie, leider falsch..[/bold red]")
					errors += 1

			timee = round(time.time() - start)
			penalty = errors * self.error_penalty
			total = timee + penalty
			points = max(self.points_max_time - total, 0)

			self.balancer.update(calc, points)
			self.last = calc

			total_time += timee
			total_errors += errors
			total_points += points

		print()
		print("Zeit gebraucht: {}s".format(total_time))
		print("Fehler: {} -> +{}s".format(total_errors, self.error_penalty * total_errors))
		print("Punkte total: {}s".format(total_points))

		s = Stat(total_time, total_points)
		self.stats.add(s)
		self.balancer.end_session()

		return Result(
			name=self.name,
			questions=self.num_calcs,
			correct=self.num_calcs - total_errors,
			time=total_time,
			points=total_points
		)
