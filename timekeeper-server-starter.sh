#!/bin/bash

cd /home/pi/timekeeper-mqtt/
git pull
source venv/bin/activate
pip3 install -r requirements.txt
python3 timekpr-server-starter.py


